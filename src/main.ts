import Vue from 'vue';
import Amplify from 'aws-amplify';
import App from './App.vue';
import router from './router';
import store from './store';
import awsconfig from './aws-exports';
import '@aws-amplify/ui-vue';

Amplify.configure(awsconfig);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
